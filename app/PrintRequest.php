<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;



class PrintRequest extends Model
{
    protected $table = 'requests';

    protected $fillable = [
    'id','owner_id', 'status', 'due_date', 'description', 'quantity', 'colored',
    'stapled', 'paper_size', 'paper_type', 'file', 'printer_id', 'closed_date', 'closed_user_id',
    'refused_reason', 'satisfaction_grade', 'front_back',
    ];

        public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'due_date' => 'regex:/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/',
            'description' => 'max:255',
            'quantity' => 'required|regex:/^[0-9]*$/',
            'colored' => 'required|between:0,1',                //0 -- Coloured , 1 -- Black/White
            'stapled' => 'required|between:0,1',                //0 -- No , 1 -- Yes
            'paper_size' => 'required|between:3,4',             //0 -- A3, 1 -- A4, 2 -- A5
            'paper_type' => 'required|between:0,2',             //0 -- Normal, 1 -- draft, 2 -- photographic
            'file' => 'required|file'
        ];
    }

    public static function countAllRequests()
    {
        if(static::count()!=0){
            return static::count();
        }else{
            return 0;
        }
    }

    public static function countAllRequestsToDo($id)
    {
        return static::where('status', '=', '0')
        ->where('owner_id', '=', $id)
        ->count();
    }

    public static function countAllRequestsDone()
    {
        return static::where('status', '=', '1')
        ->count();
    }

    public static function countAllRequestsMy($id)
    {
        return static::where('owner_id', '=', $id)
        ->count();
    }

    public static function getAllRequestsToDo()
    {
        return static::where('status', '=', '0')
        ->get();
    }

    public static function getAllRequestsToDoQuant()
    {
        return static::where('status', '=', '0')
        ->orderBy('quantity', 'desc')
        ->get();
    }

    public static function getAllRequestsDoneQuant()
    {
        return static::where('status', '=', '1')
        ->orderBy('quantity', 'desc')
        ->get();
    }

    public static function getAllRequestsToDoType()
    {
        return static::where('status', '=', '0')
        ->orderBy('quantity', 'desc')
        ->get();
    }

    public static function getAllRequestsDoneType()
    {
        return static::where('status', '=', '1')
        ->orderBy('quantity', 'desc')
        ->get();
    }

    public static function getAllRequestsDone()
    {
        return static::where('status', '=', '1')
        ->get();
    }

    public static function ColourPercentage()
    {
        $total = self::countAllRequests();
        if($total == 0){
            return 0;
        }
        $colour = static::where('colored', '=', '1')
        ->count();
        return intval($colour/$total * 100);
    }

    public static function BlackAndWhitePercentage()
    {
        $total = self::countAllRequests();
        if($total == 0){
            return 0;
        }
        $BlAndWhite = static::where('colored', '=', '0')
        ->count();
        return intval($BlAndWhite/$total * 100);
    }

    public static function stapledVsNot()
    {
        $total = self::countAllRequests();
        if($total == 0){
            return 0;
        }
        $colour = static::where('stapled', '=', '1')
        ->count();
        return str_limit($colour/$total * 100, 5);
    }

    public static function dailyRequestsByMonth()
    {
        $startOfMonth = Carbon::today()->startOfMonth();
        $endOfMonth = Carbon::today()->endOfMonth();
        $requestsMonth = static::where('closed_date', '>=', $startOfMonth)
        ->where('closed_date', '<=', $endOfMonth)
        ->count();
        if(self::countAllRequests() == 0){
            return 0;
        }
        $total = str_limit($requestsMonth / Carbon::today()->day,5);
        return $total;
    }

    public static function dailyRequests()
    {
        $now = Carbon::now();
        $total = static::whereDay('closed_date', $now->day)
        ->whereMonth('closed_date', $now->month)
        ->whereYear('closed_date', $now->year)
            ->count();// https://github.com/briannesbitt/Carbon/issues/317
            return $total;
        }

        public static function departs($id)
        {
            return self::join('users', 'users.id', '=', 'requests.owner_id')
            ->join('departments', 'departments.id', '=', 'users.department_id')
            ->where('departments.id', $id)
            ->count();
        }

        public static function highestUser()
        {
            return self::select(DB::raw('count(owner_id) as countOfId, owner_id'))
            ->groupBy('owner_id')
            ->orderBy('countOfId', 'desc')
            ->pluck('owner_id')
            ->first();
        }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'request_id')->whereNull('parent_id');
    }

}