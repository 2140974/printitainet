<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('common.head')
</head>

<body>
	<header>
		@include('common.header')
	</header>
	<section style="padding:100px">
		<br>
		<h3>Resultados:</h3>
		<br>
		@if(Auth::check())        
		<table class = "table table-striped" >
			<thead>
				<tr>
					<th>ID do Dono</th>
					<th>Data abertura</th>
					<th>Data limite</th>
					<th>Quantidade</th>
					<th>Cores</th>
					<th>Agrafado</th>
					<th>Tamanho folha</th>
					<th>Tipo papel</th>
					<th>Ficheiro</th>
				</tr>
			</thead>
			@foreach ($queryFinal as $request)
			@if($request->id == Auth::id() || $request->status == 1)
			<tbody>
				<tr>
					<td>{{$request->id}}</td>
					<td>{{$request->created_at}}</td>
					<td>{{$request->due_date}}</td>
					<td>{{$request->quantity}}</td>
					@if($request->colored == 0)
					<td>Preto/Branco</td>
					@else
					<td>Cores</td>
					@endif
					@if($request->stapled == 0)
					<td>Não</td>
					@else
					<td>Sim</td>
					@endif
					<td>{{$request->paper_size}}</td>
					<td>{{$request->paper_type}}</td>
					<td>{{$request->file}}</td>
					<td>
						<a type="submit" class="btn btn-default" href="{{route('request.showDetail', $request)}}">Detalhes</a>
					</td>
				</tr>
			</tbody>
			@endif
			@endforeach
		</table>
		@else
		<h2>Não foram encontrados pedidos.</h2>
		@endif		
	</section>
	<br>
	<footer style="position:absolute; width:100%; height:60px">
		@include('common.footer')
	</footer>
</body>
</html>