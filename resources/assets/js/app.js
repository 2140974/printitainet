$(document).ready(function () {
    $(".dropdown-toggle").dropdown();

    $(".btn-reply").on('click', function() {
        var reply = $(this).parent().siblings('.reply');

        if (reply.hasClass('hide')) {
            reply.removeClass('hide')
        } else {
            reply.addClass('hide');
        }
    });
});