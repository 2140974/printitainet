<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('common.head')
</head>
<body>
    <header>
        @include('common.header')
    </header>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
          <h2>Criar Pedido</h2>
          <form action="{{route('request.store', $requestClass)}}" method="post" class="form-group">
            {{csrf_field()}}
            @include('request.partials.add-edit')
            <div class="form-group">
                <button type="submit" class="btn btn-success" name="ok">Adicionar</button>
                <button class="btn btn-default" href="{{route('request.showDetail', $requestClass)}}">Cancelar</button>
            </div>
        </form>
        <footer style="position:absolute; width:100%; height:60px">
            @include('common.footer')
        </footer>
    </body>
    </html>