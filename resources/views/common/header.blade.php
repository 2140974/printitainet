        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            PrintIT
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            <li><a href="/">Home</a></li>
                            <li><a href="{{ route('request.index')}}">Pedidos</a></li>
                            <li><a href="/contactos">Contactos</a></li>
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a style="background-color:#0099ff; color:white" href="{{ route('login') }}">Login</a></li>
                            <li><a style="background-color:#0099ff; color:white" href="{{ route('register') }}">Registar</a></li>
                            @else
                            <li><a style="background-color:#0099ff; color:white" href="{{route('users.edit', Auth::user())}}">Editar Perfil</a></li>
                            <li><a style="background-color:#0099ff; color:white" href="{{route('logout')}}">Logout</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>

            @yield('content')
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>