@component('mail::message')
# Introduction

Activate your printIT account!

@component('mail::button', ['url' => $url])
Email activation
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
