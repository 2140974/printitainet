<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use  Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'admin', 'blocked', 'phone', 'profile_photo',
        'profile_url', 'presentation', 'department_id', 'token'
    ];

    public $timestamps = true;

    protected $hidden = [
    'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(30);
        });
    }

    /**
     * Check if user is administrator.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->admin === 1;
    }
    /**
     * Check if user is blocked.
     *
     * @return bool
     */
    public function isBlocked()
    {
        return $this->blocked === 1;
    }

    public function notActivated()
    {
        return $this->activated === 0;
    }

    public function confirmEmail()
    {
        $this->blocked = 0;

        $this->token = null;

        $this->activated = 1;

        $this->save();
    }
          public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id');
    }

    public function departmentStr()
    {
        return $this->hasOne(Department::class, 'id', 'department_id' );
    }

    public function requests()
    {
        return $this->hasMany(Request::class, 'owner_id', 'id');
    }
    

}
