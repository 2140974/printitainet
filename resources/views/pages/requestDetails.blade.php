<!DOCTYPE html>
<html>
<head>
    @include('common.head')
</head>
<body>
    <header>
        @include('common.header')
    </header>
    <section style="padding:100px">
        <a type="submit" class="btn btn-primary" href="{{route('request.index')}}">Voltar</a>
        <br>
        <br>
        <caption>Pedido de impressão:</caption>
        <table class = "table table-striped" >
            <tbody>
                <tr>
                    <td>ID do Dono</td>
                    <td>{{$requestClass->id}}</td>
                </tr>
                <tr>
                    <td>Estado</td>
                    @if($requestClass->status == 0)
                    <td>Por imprimir!</td>
                    @else
                    <td>Imprimido!</td>
                    @endif
                </tr>
                <tr>
                    <td>Data abertura</td>
                    <td>{{$requestClass->created_at}}</td>
                </tr>
                <tr>
                    <td>Data limite</td>
                    <td>{{$requestClass->due_date}}</td>
                </tr>
                <tr>
                    <td>Descrição</td>
                    <td>{{$requestClass->description}}</td>
                </tr>
                <tr>
                    <td>Quantidade</td>
                    <td>{{$requestClass->quantity}}</td>
                </tr>
                <tr>
                    <td>Cores</td>
                    @if($requestClass->colored == 0)
                    <td>Preto/Branco</td>
                    @else
                    <td>Cores</td>
                    @endif
                </tr>
                <tr>
                    <td>Agrafado</td>
                    @if($requestClass->stapled == 0)
                    <td>Não</td>
                    @else
                    <td>Sim</td>
                    @endif
                </tr>
                <tr>
                    <td>Tamanho folha</td>
                    <td>{{$requestClass->paper_size}}</td>
                </tr>
                <tr>
                    <td>Tipo papel</td>
                    <td>{{$requestClass->paper_type}}</td>
                </tr>
                <tr>
                    <td>Ficheiro</td>
                    <td>{{$requestClass->file}}</td>
                </tr>
                <tr>
                    <td>ID impressora</td>
                    <td>{{$requestClass->printer_id}}</td>
                </tr>
                <tr>
                    <td>Data expiração</td>
                    <td>{{$requestClass->closed_date}}</td>
                </tr>
                <tr>
                    <td>ID utilizador que recusou</td>
                    <td>{{$requestClass->closed_user_id}}</td>
                </tr>
                <tr>
                    <td>Razão de recusa</td>
                    <td>{{$requestClass->refused_reason}}</td>
                </tr>
                <tr>
                    <td>Satisfação</td>
                    <td>{{$requestClass->satisfaction_grade}}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>

             @if (Auth::check())
                <div class="widget-area">
                    <div class="status-upload">
                        <form action="{{ url("request/{$requestClass->id}/comment") }}" method="POST">
                            {!! csrf_field() !!}
                            <textarea name="comment" id="comment" placeholder="Please, give us your comment.."></textarea>
                            <button type="submit" class="btn btn-success green">
                                <i class="fa fa-share"></i>
                                Comment
                            </button>
                        </form>
                    </div>
                </div>

                <hr class="normal">
            @endif

            <div class="panel panel-default widget">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span>
                    <h3 class="panel-title">
                        Recent Comments</h3>
                </div>

                <div class="panel-body">
                    <ul class="list-group">
                        @foreach ($requestClass->comments as $comment)
                            @if (!$comment->blocked || ($comment->blocked && Auth::check() && Auth::user()->isAdmin()))
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div>
                                                <div class="mic-info">
                                                    By: <a href="#">{{ $comment->users->name }}</a> on
                                                    {{ $comment->created_at->toFormattedDateString() }}
                                                </div>
                                            </div>
                                            <div class="comment-text">
                                                {{ $comment->comment }}
                                            </div>
                                            <div style="padding-left: 25px;">
                                                @foreach ($comment->replies as $reply)
                                                    <div style="padding-top: 10px;">
                                                        <div class="mic-info">
                                                            By: <a href="#">{{ $reply->users->name }}</a> on
                                                            {{ $reply->created_at->toFormattedDateString() }}
                                                            <div class="pull-right">
                                                                @if (Auth::check() && Auth::user()->isAdmin())
                                                                    @if ($reply->blocked == 1)
                                                                        <a href="{{ url("request/{$requestClass->id}/comment/{$reply->id}/restore") }}" class="btn btn-success btn-xs" title="Block">
                                                                            <span class="glyphicon glyphicon-ok"></span>
                                                                        </a>
                                                                    @else
                                                                        <a href="{{ url("request/{$requestClass->id}/comment/{$reply->id}/block") }}" class="btn btn-danger btn-xs" title="Block">
                                                                            <span class="glyphicon glyphicon-ban-circle"></span>
                                                                        </a>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="comment-text">
                                                        {{ $reply->comment }}
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="action pull-right">
                                                <button type="button" class="btn btn-reply btn-primary btn-xs" title="Reply">
                                                    <span class="glyphicon glyphicon-share-alt"></span>
                                                </button>

                                                @if (Auth::check() && Auth::user()->isAdmin())
                                                    @if ($comment->blocked == 1)
                                                        <a href="{{ url("request/{$requestClass->id}/comment/{$comment->id}/restore") }}" class="btn btn-success btn-xs" title="Restore">
                                                            <span class="glyphicon glyphicon-ok"></span>
                                                        </a>
                                                    @else
                                                        <a href="{{ url("request/{$requestClass->id}/comment/{$comment->id}/block") }}" class="btn btn-danger btn-xs" title="Block">
                                                            <span class="glyphicon glyphicon-ban-circle"></span>
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>
                                            @if (Auth::check())
                                                <div class="widget-area reply hide" style="margin-top: 10px;">
                                                    <div class="status-upload">
                                                        <form action="{{ url("request/{$requestClass->id}/comment/{$comment->id}") }}" method="POST">
                                                            {!! csrf_field() !!}
                                                            <textarea name="comment" id="comment" placeholder="Please, give us your reply.."></textarea>
                                                            <button type="submit" class="btn btn-success green">
                                                                <i class="fa fa-share"></i>
                                                                Comment
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="row">

    <div class="form-group">
        <button type="submit" class="btn btn-success" name="ok">Editar</button>
        <button class="btn btn-default" href="{{route('users.index')}}">Voltar</button>
    </div>
    <script src="http://code.ainet/js/app.js" type="text/javascript"></script>


    <footer style="position:absolute; bottom:0; width:100%; height:60px">
        @include('common.footer')
    </footer>
</body>
</html>