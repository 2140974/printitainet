@extends('layouts.app')

@section('title', 'User Profile')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="thumbnail">
                <!-- <img class="img-responsive" src="http://placehold.it/800x300" alt=""> -->
                <div class="caption-full">
                    @if (Auth::check() && Auth::user()->isAdmin())
                        @if ($user->blocked === 0)
                            <div class="text-left">
                                <a class="btn btn-sm btn-danger" href="{{ url("users/block/{$user->id}") }}">
                                    <span class="glyphicon glyphicon-ban-circle"></span>
                                    Blocked
                                </a>
                            </div>
                        @endif
                        @endif
                    <img src="/uploads/avatars/{{ $user->profile_photo }}" style="width:100px; height:100px; float:left; border-radius:50%; margin-right:25px;">
                   <h2 style="color: #31b0d5;">{{ $user->name }}</h2>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div><h4><Strong>Description:</Strong></h4></div>
                            @if($user->presentation != null)
                                <div>{{ $user->presentation }}</div>
                            @else
                                <div>Description not available</div>
                            @endif

                       
                            <div><h4><Strong>Email:</Strong></h4></div>
                            <div>{{ $user->email }}</div>

                            <div><h4><Strong>Department :</Strong></h4></div>
                            <div> {{ $user->departmentStr->name }}</div>
                        </div>       
                </div>
            </div>
        </div>
    </div>
@endsection