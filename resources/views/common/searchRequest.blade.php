<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pesquisar</div>
                <div class="panel-body">
                    <article class="box col-1">
                        <section class="box">
                            <div class="form-group">
                                <label for="inputDescription">Nome</label>
                                <input
                                type="text" class="form-control"
                                name="NomeFunc" id="NomeFunc"
                                placeholder="Nome"/>
                            </div>
                            <div class="form-group">
                                <label for="inputDescription">Data do Impressão</label>
                                <input
                                type="date" class="form-control"
                                name="closed_date" id="closed_date"
                                placeholder="dd - mm - aaaa"/>
                            </div>
                            <div class="form-group">
                                <label for="inputType">Cores</label>
                                <select name="colored" id="colored" class="form-control">
                                    <option disabled selected> -- selecione uma opção -- </option>
                                    <option value="0">Preto e branco</option>
                                    <option value="1">Cores</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="department_id">Departamento</label>
                                <select name="department_id" id="department_id" class="form-control">
                                  <option disabled selected> -- Selecione uma opção -- </option>
                                  <option value="1">Ciências Jurídicas</option>
                                  <option value="2">Ciências da Linguagem</option>
                                  <option value="3">Engenharia do Ambiente</option>
                                  <option value="4">Engenharia Civil</option>
                                  <option value="5">Engenharia Eletrotécnica</option>
                                  <option value="6">Engenharia Informática</option>
                                  <option value="7">Engenharia Mecânica</option>
                                  <option value="8">Gestão e Economia</option>
                                  <option value="9">Matemática</option>
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="inputType">Estado</label>
                            <select name="status" id="status" class="form-control">
                                <option disabled selected> -- Selecione uma opção -- </option>
                                <option value="0">Por Imprimir</option>
                                <option value="1">Impresso</option>
                            </select>
                        </div>
                        <form action="{{route('request.search')}}" method="get" class="form-group">
                        <button type="submit" class="btn btn-primary" href="{{route('request.search')}}" >Pesquisar</button>
                        </form>
                    </section>
                </article>
            </div>
        </div>
    </div>
</div>
</div>