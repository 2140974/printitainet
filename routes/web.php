<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




//Routes users

Auth::routes();

Route::get('logout', function (){
Auth::logout();
return redirect('/');
});
Route::get('users', 'UserController@index')->name('users');
Route::get('all/users','UserController@allUsers')->name('all.users');
Route::get('user/show/profile/{id}', 'UserController@showProfile')->name('user.showProfile');
Route::get('user/edit/profile','UserController@edit')->name('user.editProfile');
Route::get('users/{id}/edit','UserController@edit')->name('users.edit');
Route::post('user/update/profile','UserController@update')->name('user.updateProfile');
Route::get('all/users', 'UserController@allUsers')->name('users.index');


Route::get('/users/confimation/{token}', 'Auth\RegisterController@confirmEmail')->name('confirmation');

//Routes admin
Route::get('user/admin/{id}', 'UserController@addAdmin')->name('user.admin');
Route::get('user/admin/remove/{id}', 'UserController@pullAdmin')->name('user.removeAdmin');
Route::get('admin/users', 'AdminController@viewAdmin')->name('admin.users');

//Routes comments
Route::post('request/{requestId}/comment', 'RequestController@comment')->name('request.comment');
Route::post('request/{requestId}/comment/{commentId}', 'RequestController@reply')->name('request.commentID');
Route::get('request/{requestId}/comment/{commentId}/block', 'RequestController@blockComment')->middleware('auth')->name('request.comment.block');
Route::get('request/{requestId}/comment/{commentId}/restore', 'RequestController@restoreComment')->middleware('auth')->name('request.comment.restore');


//Request Routes
Route::get('request/create', 'RequestController@create')->name('request.create');
Route::post('/request/create', 'RequestController@store')->name('request.store');
Route::get('/request', 'RequestController@index')->name('request.index');
Route::get('/requestQ', 'RequestController@indexQ')->name('request.indexQ');
Route::get('/requestT', 'RequestController@indexT')->name('request.indexT');
Route::get('/request/{request}', 'RequestController@showDetail')->name('request.showDetail');
Route::get('/request/{request}/edit', 'RequestController@edit')->name('request.edit');
Route::delete('/request/{request}', 'RequestController@destroy')->name('request.destroy');
Route::get('/request/search', 'RequestController@search')->name('request.search');
Route::get('/request/refuse', 'RequestController@refuse')->name('request.refuse');
Route::get('/request/print', 'RequestController@print')->name('request.print');
//Home Route
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function()
    {
        return View::make('pages.home');
    });
