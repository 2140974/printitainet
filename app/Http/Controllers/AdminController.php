<?php

namespace App\Http\Controllers;

use Auth;
use App\Advertisement;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::user()->isAdmin()) {
            return redirect('/');
        }

        $advertisements = Advertisement::paginate(15);

        return view('product.admin-advertisementList', ['advertisements' => $advertisements]);
    }

    public function viewAdmin(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            return redirect('/');
        }

        $data = [];

        if ($request->has('search')) {
            $search = $request->get('search');
            $users = User::where("name", "like", "{$search}%")
                   ->paginate(15);
            $data['search'] = $search;
        } else {
            $users = User::paginate(15);
            $data['search'] = '';
        }

        $data['users'] = $users;

        return view('users.admin-index', $data);
    }
}
