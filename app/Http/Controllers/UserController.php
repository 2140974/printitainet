<?php

namespace App\Http\Controllers;

use Auth;
use App\Advertisement;
use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
    public function index()
    {
        $users = User::where('blocked', 0)->paginate(15);

        return view('users.index', ['users' => $users]);
    }



    public function allUsers()
    {
        $users = User::paginate(10);
        return view ('users.index', ['users' => $users]);
    }

    public function edit()
    {
        $user = Auth::user();

        return view('users.edit', ['user' => $user]);
    }



    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
        ]);

        $user = Auth::user();

        $user->name = $request->get('name');

        $user->save();

        session()->flash('message', 'The profile was update successfully.');

        return redirect('users');
    }

    public function blocked($id)
    {
        $users = User::find($id);

        $users->blocked = 1;

        $users->save();

        session()->flash('message', 'The user was blocked successfully.');

        return redirect('admin/users');
    }

    public function restore($id)
    {
        $users = User::find($id);

        $users->blocked = 0;

        $users->save();

        session()->flash('message', 'The user was restore successfully.');

        return redirect('admin/users');
    }

    public function addAdmin($id)
    {
        $users = User::find($id);

        $users->admin = 1;

        $users->save();

        session()->flash('message', 'The user are now an administrator.');

        return redirect('admin/users');
    }

    public function pullAdmin($id)
    {
        $users = User::find($id);

        $users->admin = 0;

        $users->save();

        session()->flash('message', 'The user are not administrator anymore.');

        return redirect('admin/users');
    }

    public function showProfile($id)
    {
        $user = User::where('blocked', 0)
                    ->where('id', $id)
                    ->firstOrFail();

        return view('users.show-profile', ['user' => $user]);
    }

    public function getEdit()
    {
        return view('users.partials.add-edit');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required',
            'department_id' => 'required|int|',

        ]);

        $user = new User();

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->department_id = $request->get('department_id');

        $user->save();

        return redirect('users');
    }
}

