<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Urban Farmers Market</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url('products') }}">Products</a>
                </li>
                <li>
                    <a href="{{ route('users') }}">Participants</a>
                </li>
                @if (Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Trade Dashboard
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown"><a href="{{ route('advertisements') }}">Advertisements</a></li>
                        </ul>
                    </li>
                @endif
                @if (Auth::check() && Auth::user()->isAdmin())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Market Admin
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('admin/advertisements') }}">Advertisements</a></li>
                            <li><a href="{{ url('admin/users') }}">Users</a></li>
                            <li><a href="{{ url('tags') }}">Tags</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li>
                        <a href="{{ url('auth/register') }}" id="nav-register">Register</a>
                    </li>
                    <li>
                        <a href="{{ url('auth/login') }}" id="navLogin">Login2</a>
                    </li>
                @else
                    <li class="dropdown" id="user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-user"></span>
                            {{ Auth::user()->name }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ url('user/edit/profile') }}">
                                    <span class="glyphicon glyphicon-user"></span>
                                    Profile
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('auth/logout') }}">
                                    <span class="glyphicon glyphicon-log-out"></span>
                                    Log Out
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>