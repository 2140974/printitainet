<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('common.head')
</head>

<body>
	<header>
		@include('common.header')
	</header>
	<div class="form-group">
		<label for="inputDescription">Motivo</label>
		<input
		type="textarea" class="form-control"
		name="refused_reason" id="refused_reason"
		placeholder="Motivo"></input>	
	</div>
	<form action="{{route('request.refuseFinish', $requestClass)}}" method="put" class="form-group">
			<button type="submit" class="btn btn-success" href="{{route('request.refuseFinish', $requestClass)}}">Recusar</button>
			<button class="btn btn-default" href="{{route('request.index')}}">Voltar</button>        
		</form>
	<footer style="position:absolute; width:100%; height:60px">
		@include('common.footer')
	</footer>
</body>
</html>