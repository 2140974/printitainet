@extends('layouts.app')

@section('title', 'List users')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <form action="{{ url('admin/users') }}" method="GET">
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control" name="search"
                           placeholder="Search by name..." value="{{ isset($search) ? $search : ''}}">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    @if (session()->has('message'))
        <div class="col-xs-12">
            <div class="alert alert-success">{{ session('message') }}</div>
        </div>
    @endif

    @if (count($users))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Name</th>


            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>

                    <td>{{ $user->name }}</td>

                    <td>
                        @if (Auth::check() && Auth::user()->isAdmin() && $user->blocked === 0)
                            <a class="btn btn-sm btn-primary" href="{{ url('users/block/'.$user->id) }}">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                Blocked
                            </a>
                        @endif
                        @if (Auth::check() && Auth::user()->isAdmin() && $user->blocked === 1)
                            <a class="btn btn-sm btn-success" href="{{ url('users/restore/'.$user->id) }}">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                Restore
                            </a>
                        @endif
                        @if (Auth::check() && Auth::user()->isAdmin() && !$user->isAdmin())
                            <a class="btn btn-sm btn-danger" href="{{ url('user/admin/'.$user->id) }}">
                                <span class="glyphicon glyphicon-ban-circle"></span>
                                Turn Admin
                            </a>
                        @else
                            <a class="btn btn-sm btn-success" href="{{ url('user/admin/remove/'.$user->id) }}">
                                <span class="glyphicon glyphicon-ok-circle"></span>
                                Revoke Admin
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
        <div class="row">
            <div class="col-xs-offset-4 col-xs-4">
                {!! $users->appends(['search', $search])->render() !!}
            </div>
        </div>
    @else
        <h2>No users found</h2>
    @endif
@endsection
