<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Urban Farmers - @yield('title', 'Home')</title>
    <!-- CSS files -->
    <link href="{{ asset("css/bootstrap.css") }}" rel="stylesheet">
    <link href="{{ asset("css/app.css") }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    @include('layouts.partials.navbar')

    <div class="container">
        @yield('content')
    </div>

    <div class="container">
        <hr class="normal">
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>&copy; Urban Farmers Market {{ date('Y') }}</p>
                </div>
            </div>
        </footer>
    </div>

    <!-- Javascript files -->
    <script src="{{ asset("js/jquery.js") }}" type="text/javascript"></script>
    <script src="{{ asset("js/bootstrap.js") }}" type="text/javascript"></script>

    <script src="{{ asset("js/app.js") }}" type="text/javascript"></script>
</body>
</html>