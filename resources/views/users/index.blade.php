@extends('layouts.app')

@section('title', 'List users')

@section('content')

@if (session()->has('message'))
<div class="col-xs-12">
    <div class="alert alert-success">{{ session('message') }}</div>
</div>
@endif

@if (count($users))
<table class="table table-striped">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Name<span>@if(Request::route()->getName() == 'users' || Request::is('users/name/asc'))
                <a class="glyphicon glyphicon-sort" href="{{url('users/name', 'desc')}}"></a></span></th>
                @else
                <a class="glyphicon glyphicon-sort" href="{{url('users/name', 'asc')}}"></a></span></th>
                @endif
                <th>Departamento<span>
                    @if(Request::route()->getName() == 'users' || Request::is('users/location/asc'))
                    <a class="glyphicon glyphicon-sort" href="{{url('users/location', 'desc')}}"></a></span></th>
                    @else
                    <a class="glyphicon glyphicon-sort" href="{{url('users/location', 'asc')}}"></a></span></th>

                    @endif
                    <th>Print Evals</th>
                    <th>Print Counts</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    @if($user->profile_photo != null)
                    <td><img src="/uploads/avatars/{{ $user->profile_photo }}" style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;"></td>   
                    @else
                    <td> Image not available  </td>   
                    @endif


                    <td>{{ $user->name }}</td>
                    <td>{{ $user->department_id }}</td>
                    <td>{{ $user->print_evals }}</td>
                    <td>{{ $user->print_counts }}</td>
                    @if(Auth::check() && Auth::user()->isAdmin())

                    @endif
                    <td><a class="btn btn-sm btn-primary" href="{{ url('user/advertisements/'.$user->id)}}" >
                        <span class="glyphicon glyphicon-new-window"></span>
                        Impressoes
                    </a>
                </td>
                <td><a class="btn btn-sm btn-primary" href="{{ url('user/show/profile/'.$user->id)}}" >
                    <span class="glyphicon glyphicon-user"></span>
                    Profile
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div class="row">
    <div class="col-xs-offset-4 col-xs-4">
        {!! $users->render() !!}
    </div>
</div>
@else
<h2>No users found</h2>
@endif
@endsection
