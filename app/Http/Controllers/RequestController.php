<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PrintRequest;
use Auth;
use App\Http\Requests\StoreRequestRequest;
use App\Http\Requests\UpdateRequestRequest;
use Hash;
use App\Comment;
use Carbon\Carbon;



class RequestController extends Controller
{
    public function index()
    {
        $requestClass = request()->all();
        return view('pages.request', compact('requestClass'));
    }

    public function showDetail(PrintRequest $request)
    {
        $requestClass = PrintRequest::findOrFail($request->id);
        return view('pages.requestDetails', compact('requestClass'));
    }

    public function indexQ()
    {
        $requestClass = PrintRequest::all();
        return view('pages.requestQ', compact('requestClass'));
    }

    public function indexT()
    {
        $requestClass = PrintRequest::all();
        return view('pages.requestT', compact('requestClass'));
    }

    public function create()
    {
        $requestClass = new PrintRequest();
        return view('request.add', compact('requestClass'));
    }

    public function store(Request $request)
    {
        $requestClass = new PrintRequest();
        $requestClass->status = 0;
        $requestClass->owner_id = Auth::id();
        $requestClass->fill($request->all());
        $requestClass->save();


        return redirect()->route('request.index')->with('success', 'Request created successfully!');
    }

    public function edit(Request $requestClass)
    {
        return view('request.edit', compact('$requestClass'));
    }

    public function update(UpdateRequestRequest $request, PrintRequest $requestClass)
    {

        $requestClass->fill($request->except('password'));
        $requestClass->save();

        return redirect()->route('request.index')->with('success', 'Request updated successfully!');
    }

    public function comment(Request $request, $requestId)
    {
        $this->validate($request, [
           'comment' => 'required'
           ]);

        $text = $request->get('comment');

        $comment = $this->startComment($text, $requestId);

        $comment->save();
        return redirect("request/{$requestId}");
    }

    public function search()
    {
        $query = DB::table('request');

        if (!empty($_POST['NomeFunc'])) {
            $query = $query->whereIn('owner_id', function($sub){ 
                $sub->select('id')
                ->from('users')
                ->where('name', 'LIKE', htmlspecialchars('%' , $_POST['NomeFunc']));
            });
        }

        if (!empty($_POST['status'])) {
            $query = $query->where('status', $_POST['status']);
        }

        if (!empty($_POST['closed_date'])) {
            $query = $query->whereRaw('DATE(closed_date) LIKE ?', $_POST['closed_date']);
        }

        if (!empty($_POST['dep'])) {
            $query = $query->where('id',function($sub){ 
                $sub->select('id')
                ->from('departments')
                ->where('id', '=', $_POST['dep']);
            });
        }

        $queryFinal = $query->get();

        return view('pages.requestSearch', compact('queryFinal'));

    }

    public function refuse(PrintRequest $request)
    {
        return view('pages.refuse', compact('requestClass'));
    }

    public function refuseFinish(PrintRequest $requestClass, Request $request)
    {

        $requestClass->refused_reason = $request->get('refused_reason');
        $requestClass->status = 1;
        $requestClass->closed_date = Carbon::now();
        $requestClass->save();

        return redirect()->route('request.index')->with('success', 'Request refused successfully!');
    }

    public function print(PrintRequest $request)
    {
        return view('pages.print', compact('requestClass'));
    }

    public function printFinish(PrintRequest $request)
    {
        $requestClass->satisfaction_grade = $request->get('satisfaction_grade');
        $requestClass->printer_id = $request->get('printer_id');
        $requestClass->closed_user_id = $request->get('closed_user_id');
        $requestClass->status = 1;
        $requestClass->closed_date = Carbon::now();
        $requestClass->save();

        return redirect()->route('request.index')->with('success', 'Request refused successfully!');
    }

    public function reply(Request $request, $requestId, $commentId)
    {
        $this->validate($request, [
            'comment' => 'required'
            ]);

        $text = $request->get('comment');

        $comment = $this->startComment($text, $requestId);
        $comment->parent_id = $commentId;
        
        $comment->save();



        return redirect("request/{$requestId}");
    }

    public function startComment($text, $id)
    {
        $comment = new Comment();

        $comment->comment = $text;
        $comment->request_id = $id;
        $comment->user_id = Auth::id();

        return $comment;
    }

    public function blockComment($requestId, $commentId)
    {
        $this->changeCommentStatus($commentId, true);

        return redirect("request/{$requestId}");
    }

    public function restoreComment($requestId, $commentId)
    {
        $this->changeCommentStatus($commentId);

        return redirect("request/{$requestId}");
    }

    public function changeCommentStatus($commentId, $status = false)
    {
        if (!Auth::user()->isAdmin()) {
            return response("Unauthorized. You not allow to make this action", 401);
        }

        $comment = Comment::findOrFail($commentId);

        $comment->blocked = $status;

        $comment->save();
    }
    
    public function destroy(PrintRequest $requestClass, $request)
    {        
        $requestClass = PrintRequest::findOrFail($request);
        $requestClass->delete();

        return redirect()
        ->route('request.index')
        ->with('success', 'Request deleted successfully');
    }
}
