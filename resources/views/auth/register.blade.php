@extends('layouts.app')

@section('title', 'Register')

@section('content')

@if (session()->has('message'))
<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
 <div class="alert alert-info">{{ session('message') }}</div>
</div>
@endif
<div class="row">
  <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <h2>Register</h2>
    <form role="form" action="{{ url('register') }}" method="POST">
      <hr class="colorgraph">
      {!! csrf_field() !!}
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group @if ($errors->has('name')) has-error @endif">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="Name"  value="{{ old('name') }}" tabindex="1">
            <span class="help-block">{{ $errors->first('name') }}</span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="form-group @if ($errors->has('name')) has-error @endif">
            <label for="phone">phone</label>
            <input type="number" name="phone" id="phone" class="form-control" placeholder="phone"  value="{{ old('phone') }}" tabindex="1">
            <span class="help-block">{{ $errors->first('phone') }}</span>
          </div>
        </div>
      </div>
      <div class="form-group @if ($errors->has('email')) has-error @endif">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" value="{{ old('email') }}" tabindex="3">
        <span class="help-block">{{ $errors->first('email') }}</span>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group @if ($errors->has('password')) has-error @endif">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="Password" tabindex="4">
            <span class="help-block">{{ $errors->first('password') }}</span>
          </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
          <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" tabindex="5">
            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
          </div>
        </div>
      </div>
      <div class="form-group">
        <label for="department_id">Department</label>
        <select name="department_id" id="department_id" class="form-control">
          <option disabled selected> -- select an option -- </option>
          @foreach ($departments as $department)
          <option value = "{{$department->id}}"> {{ $department->name}}</option>
          @endforeach
        </select>
      </div>
      <hr class="colorgraph">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <input type="submit" name="register" href="{{ url('register/confirm') }}" id="register" value="Register" class="btn btn-primary btn-block" tabindex="6">
        </div>
      </div>
    </form>
  </div>
</div>
@endsection