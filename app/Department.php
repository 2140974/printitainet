<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

	protected $fillable = [
	'id','name', 'created_at', 'updated_at',
	];

	public static function getAllDepartments()
    {
        return static::where('id', '>', '0')
            ->get();
    }
}
