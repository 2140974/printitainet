<div class="form-group">
    <label for="inputDescription">Descrição</label>
    <input
    type="text" class="form-control"
    name="description" id="description"
    placeholder="Descrição" value="{{old('description', $requestClass->description)}}" />
</div>
<div class="form-group">
    <label for="inputDescription">Data do Limite</label>
    <input
    type="date" class="form-control"
    name="due_date" id="due_date"
    placeholder="dd - mm - aaaa" value="{{old('due_date', $requestClass->due_date)}}" />
</div>
<div class="form-group">
    <label for="inputDescription">Quantidade</label>
    <input
    type="number" class="form-control"
    name="quantity" id="quantity"
    placeholder="Quantidade" value="{{old('quantity', $requestClass->quantity)}}" />
</div>
<div class="form-group">
    <label for="inputType">Cores</label>
    <select name="colored" id="inputType" class="form-control">
        <option disabled selected> -- selecione uma opção -- </option>
        <option value="0">Preto e branco</option>
        <option value="1">Cores</option>
    </select>
</div>
<div class="form-group">
    <label for="inputType">Agrafo</label>
    <select name="stapled" id="inputType" class="form-control">
        <option disabled selected> -- selecione uma opção -- </option>
        <option value="0">Não Agrafado</option>
        <option value="1">Agrafado</option>
    </select>
</div>
<div class="form-group">
    <label for="inputType">Tamanho do Papel</label>
    <select name="paper_size" id="inputType" class="form-control">
        <option disabled selected> -- selecione uma opção -- </option>
        <option value="3">A3</option>
        <option value="4">A4</option>
    </select>
</div>
<div class="form-group">
    <label for="inputType">Tipo de Papel</label>
    <select name="paper_type" id="inputType" class="form-control">
        <option disabled selected> -- Selecione uma opção -- </option>
        <option value="0">Rascunho</option>
        <option value="1">Normal</option>
        <option value="2">fotográfico</option>
    </select>
</div>
<div>
    <input type="file" name="file"></input>
</div>
<div class="form-group">
    <label for="inputType">Frente e verso</label>
    <select name="paper_type" id="inputType" class="form-control">
        <option disabled selected> -- Selecione uma opção -- </option>
        <option value="0">Não</option>
        <option value="1">Sim</option>
    </select>
</div>