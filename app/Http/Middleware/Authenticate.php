<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

       if (Auth::guard($guard)->guest()) {
        if ($request->ajax() || $request->wantsJson()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->guest('auth/login');
        }
    }
    
    if (Auth::user()->isBlocked()) {
        return response('Unauthorized. Your account was blocked by the Administrator.', 401);
    }

    if (Auth::user()->notActivated()) {
        return response('Unauthorized. You must activate your account.', 401);
    }



    return $next($request);
}
}
