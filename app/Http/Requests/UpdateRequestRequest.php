<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'due_date' => 'regex:/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/',
            'description' => 'max:255',
            'quantity' => 'required|regex:/^[0-9]*$/',
            'colored' => 'required|between:0,1',                //0 -- Coloured , 1 -- Black/White
            'stapled' => 'required|between:0,1',                //0 -- No , 1 -- Yes
            'paper_size' => 'required|between:0,2',             //0 -- A3, 1 -- A4, 2 -- A5
            'paper_type' => 'required|between:0,2',             //0 -- Normal, 1 -- draft, 2 -- photographic
            'file' => 'required|file'
        ];
    }
}
