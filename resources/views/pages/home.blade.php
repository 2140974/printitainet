<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('common.head')

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {'packages':['corechart']});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {

			var data = google.visualization.arrayToDataTable([
				['Task', 'Percentagem'],
				['Cor', {{App\PrintRequest::ColourPercentage()}}],
				['Preto e branco:', {{App\PrintRequest::BlackAndWhitePercentage()}}]
				]);

			var options = {
				title: 'Percentagem de impressões a cor vs. preto e branco: '
			};

			var chart = new google.visualization.PieChart(document.getElementById('piechart'));

			chart.draw(data, options);
		}
	</script>
</head>

<body>
	<header>
		@include('common.header')
	</header>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Estatisticas</div>
					<div class="panel-body">
						<article class="box col-1">
							<section class="box">
								<p>Nº total de impressões: {{App\PrintRequest::countAllRequests()}}</p>
								<p>Percentagem de impressões agrafadas vs. não agrafadas: {{App\PrintRequest::stapledVsNot()}}</p>
								<p>Nº de impressões do dia de hoje: {{App\PrintRequest::dailyRequests()}}</p>
								<p>Média diária de impressões no mês atual: {{App\PrintRequest::dailyRequestsByMonth()}}</p>
								<table class = "table table-striped" >
									<thead>
										<tr>
											<th>Departamento</th>
											<th>Total de impressões</th>
										</tr>
									</thead>
									@foreach (App\Department::getAllDepartments() as $department)
									<tbody>
										<tr>
											<td>{{$department->name}}</td>
											<td>{{App\PrintRequest::departs($department->id)}}</td>
										</tr>
									</tbody>
									@endforeach
								</table>
							</section>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="piechart" style="width: 900px; height: 500px; margin-left: auto; margin-right: auto"></div>
	<footer style="position:absolute; width:100%; height:60px">
		@include('common.footer')
	</footer>
</body>
</html>