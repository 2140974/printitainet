@extends('layouts.app')

@section('title', 'Edit Profile')

@section('content')

@if (session()->has('message'))
<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <div class="alert alert-info">{{ session('message') }}</div>
</div>
@endif

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <h2>Edit Profile</h2>
        <form role="form" action="{{ url('user/update/profile') }}" method="POST" enctype="multipart/form-data">
            <hr class="colorgraph">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name" value="{{ old('name', $user->name) }}" tabindex="1">
                        <span class="help-block">{{ $errors->first('name') }}</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                  <div class="form-group @if ($errors->has('name')) has-error @endif">
                    <label for="phone">phone</label>
                    <input type="number" name="phone" id="phone" class="form-control" placeholder="phone"  value="{{ old('phone', $user->phone) }}" tabindex="1">
                    <span class="help-block">{{ $errors->first('phone') }}</span>
                </div>
            </div>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="form-group @if ($errors->has('name')) has-error @endif">
              <label for="profile_url">Profile URL</label>
              <input type="url" name="profile_url" id="profile_url" class="form-control" placeholder="profile_url"  alue="{{ old('phone', $user->profile_url) }}" tabindex="1">
              <span class="help-block">{{ $errors->first('profile_url') }}</span>
          </div>
      </div>
  </div>
  <div class="form-group @if ($errors->has('profile_photo')) has-error @endif">
    <label for="available_until">Profile Picture</label>
    <input type="file" name="profile_photo" id="profile_photo" class="form-control" placeholder="photo" tabindex="8">
    <span class="help-block">{{ $errors->first('profile_photo') }}</span>
</div>

<div class="form-group @if ($errors->has('presentation')) has-error @endif">
    <label for="presentation">Description</label>
    <input type="text" name="presentation" id="presentation" class="form-control" value="{{ old('presentation', $user->presentation) }}" placeholder="Description" tabindex="3">
    <span class="help-block">{{ $errors->first('presentation') }}</span>
</div>
<hr class="colorgraph">
<div class="row">
    <div class="col-xs-12 col-md-6">
        <input type="submit" name="user_edit" id="user_edit" value="Submit Changes" class="btn btn-primary btn-block" tabindex="6">
    </div>
</div>
</form>
</div>
</div>
@endsection





