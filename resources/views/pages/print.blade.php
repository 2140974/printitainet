<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('common.head')
</head>

<body>
	<header>
		@include('common.header')
	</header>

	<div class="form-group">
		<label for="inputType">Impressora</label>
		<select name="printer_id" id="inputType" class="form-control">
			<option disabled selected> -- selecione uma opção -- </option>
			@foreach ($printers as $printer)
			<option value = "{{$printer->id}}"> {{ $printer->name}}</option>
			@endforeach
		</select>
	</div>
	<div class="form-group">
		<label for="inputType">Grau de Satisfeito</label>
		<select name="satisfaction_grade" id="inputType" class="form-control">
			<option disabled selected> -- selecione uma opção -- </option>
			<option value="0">Muito Pouco Satisfeito</option>
			<option value="1">Pouco Satisfeito</option>
			<option value="0">Satisfeito</option>
			<option value="1">Muito Satisfeito</option>
			<option value="1">Totalmente Satisfeito</option>
		</select>
	</div>

	<form action="{{route('request.edit', $requestClass)}}" method="put" class="form-group">
		<button type="submit" class="btn btn-success" href="{{route('request.refuseFinish', $requestClass)}}">Imprimir</button>
		<button class="btn btn-default" href="{{route('request.index')}}">Voltar</button>        
	</form>
	<footer style="position:absolute; width:100%; height:60px">
		@include('common.footer')
	</footer>
</body>
</html>