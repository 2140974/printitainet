<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('common.head')
</head>

<body>
    <header>
        @include('common.header')
    </header>
    <section style="padding:100px">
        <form action="{{route('request.create')}}" method="get" class="form-group">
            <button type="submit" class="btn btn-primary" href="{{route('request.create')}}" >Adicionar Impressão</button>
        </form>
        @include('common.searchRequest')
        <br>
        <h3>Pedidos de impressão:</h3>
        <br>        
        @if(Auth::check())
        <h1>Impressões por fazer: </h1>
        @if(count(App\PrintRequest::countAllRequestsToDo(Auth::id()))) 
        <table class = "table table-striped" >
            <thead>
                <tr>
                    <th>ID do Dono</th>
                    <th>Data abertura</th>
                    <th>Data limite</th>
                    <th href="{{ route('request.indexQ')}}">Quantidade</th>
                    <th>Cores</th>
                    <th>Agrafado</th>
                    <th>Tamanho folha</th>
                    <th href="{{ route('request.indexT')}}">Tipo papel</th>
                    <th>Ficheiro</th>
                </tr>
            </thead>
            @foreach (App\PrintRequest::getAllRequestsToDoType() as $request)
            @if($request->id == Auth::id())
            <tbody>
                <tr>
                    <td>{{$request->id}}</td>
                    <td>{{$request->created_at}}</td>
                    <td>{{$request->due_date}}</td>
                    <td>{{$request->quantity}}</td>
                    @if($request->colored == 0)
                    <td>Preto/Branco</td>
                    @else
                    <td>Cores</td>
                    @endif
                    @if($request->stapled == 0)
                    <td>Não</td>
                    @else
                    <td>Sim</td>
                    @endif
                    <td>{{$request->paper_size}}</td>
                    <td>{{$request->paper_type}}</td>
                    <td>{{$request->file}}</td>
                    <td>
                        <a type="submit" class="btn btn-default" href="{{route('request.showDetail', $request)}}">Detalhes</a>
                    </td>
                </tr>
            </tbody>
            @endif
            @endforeach
        </table>
        @else
        <h2>Não foram encontrados pedidos.</h2>
        @endif
        @endif
        @if(count(App\PrintRequest::countAllRequestsDone()))
        <h1>Impressões já impressas: </h1>
        <table class = "table table-striped" >
            <thead>
                <tr>
                    <th>ID do Dono</th>
                    <th>Data abertura</th>
                    <th>Data limite</th>
                    <th href="{{ route('request.indexQ')}}">Quantidade</th>
                    <th>Cores</th>
                    <th>Agrafado</th>
                    <th>Tamanho folha</th>
                    <th href="{{ route('request.indexT')}}">Tipo papel</th>
                    <th>Ficheiro</th>
                </tr>
            </thead>
            @foreach (App\PrintRequest::getAllRequestsDoneType() as $request)
            <tbody>
                <tr>
                    <td>{{$request->id}}</td>
                    <td>{{$request->created_at}}</td>
                    <td>{{$request->due_date}}</td>
                    <td>{{$request->quantity}}</td>
                    @if($request->colored == 0)
                    <td>Preto/Branco</td>
                    @else
                    <td>Cores</td>
                    @endif
                    @if($request->stapled == 0)
                    <td>Não</td>
                    @else
                    <td>Sim</td>
                    @endif
                    @if($request->paper_size == 0)
                    <td>A3</td>
                    @else
                    <td>A4</td>
                    @endif
                    @if($request->paper_type == 0)
                    <td>Rascunho</td>
                    @elseif($request->paper_type == 1)
                    <td>Normal</td>
                    @else
                    <td>fotográfico</td>
                    @endif
                    <td href="storage\files\{{$request->file}}" download="{{$request->file}}">{{$request->file}}</td>
                    <td>
                        <a type="submit" class="btn btn-default" href="{{route('request.showDetail', $request)}}">Detalhes</a>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
        @else
        <h2>Não foram encontrados pedidos.</h2>
        @endif
    </section>
    <br>
    <footer style="position:absolute; width:100%; height:60px">
        @include('common.footer')
    </footer>
</body>
</html>